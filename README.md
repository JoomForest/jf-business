# Free Joomla Templates

**JF Business** is free Joomla template based on Gantry 4 framework. Also it supports **Kunena** forum Joomla Extensions and the **JomSocial** - social networking extension. Template has easy layout manager, has clean design.
The template offers some of the more original pre-created layouts that can be found in the demos. It is equipped with an additional mega menu, carousel modules, various layouts, portfolios, galleries, and Forum support.

[More Details](https://www.joomforest.com/joomla/templates/jf-business)